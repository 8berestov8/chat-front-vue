import Vue from 'vue'
import Router from 'vue-router'
import Chat from '@/components/Chat'
import ChatAuth from '@/components/ChatAuth'
import ChatReg from '@/components/ChatReg'
import ChatMain from '@/components/ChatMain'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      component: Chat,
      children: [
        {
          path: 'auth',
          component: ChatAuth
        },
        {
          path: 'reg',
          component: ChatReg
        },
        {
          path: 'main',
          component: ChatMain,
          
        }
      ]
    }
  ]
})
