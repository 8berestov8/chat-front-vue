import API from './index'

export default {
  checkAuth (uid) {
    let params = new URLSearchParams();
    
    params.append('uid', uid);
    return API.post('/users/check_auth', params);
  },
  clickAuth (login, pass){
    let params = new URLSearchParams();

    params.append('login', login);
    params.append('pass', pass);
    return API.post('/users/auth', params);
  },
  clickReg (name, login, pass) {
    let params = new URLSearchParams();

    params.append('name', name);
    params.append('login', login);
    params.append('pass', pass);
    return API.post('/users/reg', params);
  },
  clickLogAuth (uid) {
    let params = new URLSearchParams();

    params.append('uid', uid);
    return API.post('/users/log_auth', params)
  },
  clickSendMessage (message, uid, name, id_user) {
    let params = new URLSearchParams();

    params.append('date', new Date().toLocaleDateString());
    params.append('msg', message);
    params.append('uid', uid);
    params.append('name', name);
    params.append('id_user', id_user);
    return API.post('messages/add_message', params)
  },
  getMessages (uid, id_user) {
    let params = new URLSearchParams();

    params.append('id_user', id_user);
    params.append('uid', uid);
    return API.post('/messages/get_list', params)
  },
  getUsers (uid) {
    let params = new URLSearchParams();

    params.append('uid', uid);
    return API.post('/users/get_users', params)
  }
}