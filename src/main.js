// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import io from "socket.io-client";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  data: {
    name: "",
    login: null,
    id_user: null
  },
  components: { App },
  template: "<App/>",
  mounted() {
    let socket = io.connect("http://localhost:8081");
    this.socket = socket;
    socket.on("connected", data => {
      console.log(data);
    });
    socket.emit("userAuth", { event: "connected", name: "Hei" });
  }
});
